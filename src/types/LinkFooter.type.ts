export interface LinkFooter {
  url: string;
  name: string;
}
