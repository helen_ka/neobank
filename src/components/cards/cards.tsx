import React, { FC } from 'react';
import styles from './index.module.css';
import { Card } from '../../types';

interface CardsProps {
  cards: Card[];
}

const Cards: FC<CardsProps> = ({ cards }) => {
  return (
    <section className={styles.cards}>
      <div className={styles.cards_text}>
        <h1 className={styles.cards_caption}>
          Choose the design you like and apply for card right now
        </h1>
        <button className={styles.cards_chooseTheCard}>Choose the card</button>
      </div>
      <ul className={styles.cards_cardsImages}>
        {cards.map((card) => (
          <li key={card.img}>
            <div className={styles.cards_imgCards}>
              <img src={card.img} alt={card.img} />
            </div>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default Cards;
