import { ReactNode } from 'react';

export interface AboutCard {
  color: string;
  icon: ReactNode;
  caption: string;
  description: string;
}
