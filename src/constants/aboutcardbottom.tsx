import { Bag, CreditCard } from '../assets';
import React from 'react';
import { AboutCardBottom } from '../types';

export const aboutsbottom: AboutCardBottom[] = [
  {
    color: 'rgba(127, 146, 172, 0.7)',
    icon: <Bag />,
    caption: 'Up to 12 months',
    description: 'No percent. For equipment, clothes and other purchases in installments'
  },
  {
    color: '#EAECEE',
    icon: <CreditCard />,
    caption: 'Convenient deposit and withdrawal',
    description:
      'At any ATM. Top up your credit card for free with cash or transfer from other cards'
  }
];
