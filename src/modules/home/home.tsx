import React, { FC } from 'react';
import Cards from '../../components/cards';
import Benefits from '../../components/benefits';
import Currency from '../../components/currency';
import Map from '../../components/map';
import News from '../../components/news';
import Feedback from '../../components/feedback';
import { benefits } from '../../constants/benefit';
import { cards } from '../../constants/card';
import styles from './index.module.css';

interface HomeProps {}

const HomePage: FC<HomeProps> = () => {
  return (
    <>
      <div className={styles.main_body}>
        <Cards cards={cards} />
        <Benefits benefits={benefits} />
        <Currency />
        <Map />
        <News />
        <Feedback />
      </div>
    </>
  );
};

export default HomePage;
