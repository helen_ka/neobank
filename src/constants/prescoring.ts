import { PrescoringType } from '../types';

export const prescorings: PrescoringType[] = [
  {
    number: 1,
    description: 'Fill out an online application - you do not\n' + 'need to visit the bank'
  },
  {
    number: 2,
    description: 'Find out the banks decision immediately after\n' + 'filling out the application'
  },
  {
    number: 3,
    description:
      'The bank will deliver the card free of charge, wherever\n' + 'convenient, to your city'
  }
];
