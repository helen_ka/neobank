import { AboutCard } from '../types';
import React from 'react';
import { Calendar, Clock, Money } from '../assets';

export const abouts: AboutCard[] = [
  {
    color: '#EAECEE',
    icon: <Money />,
    caption: 'Up to 50 000 ₽',
    description: 'Cash and transfers without commission and percent'
  },
  {
    color: 'rgba(127, 146, 172, 0.7)',
    icon: <Calendar />,
    caption: 'Up to 160 days',
    description: 'Without percent on the loan'
  },
  {
    color: '#EAECEE',
    icon: <Clock />,
    caption: 'Free delivery',
    description: 'We will deliver your card by courier at a convenient place and time for you'
  }
];
