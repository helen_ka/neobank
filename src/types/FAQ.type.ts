export interface FAQ {
  title: string;
  content: string;
}
