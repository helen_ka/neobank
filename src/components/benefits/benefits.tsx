import React, { FC } from 'react';
import './index.module.css';
import { Illustration } from '../../assets';
import styles from './index.module.css';
import { Benefit } from '../../types';
// import { Tick } from '../../assets';

interface BenefitsProps {
  benefits: Benefit[];
}

const Benefits: FC<BenefitsProps> = ({ benefits }) => {
  return (
    <section className={styles.benefits}>
      <div className={styles.benefits_illustration}>
        <Illustration />
      </div>
      <div className={styles.benefits_textBlock}>
        <h1 className={styles.benefits_caption}>We Provide Many Features You Can Use</h1>
        <h2 className={styles.benefits_text}>
          You can explore the features that we provide with fun and have their own functions each
          feature
        </h2>
        <ul className={styles.benefits_list}>
          {benefits.map((benefit) => (
            <li key={benefit.point}>
              <div className={styles.benefits_list_item}>
                <div className={styles.benefits_list_textTick}>{benefit.point}</div>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default Benefits;
