import React, { FC, useMemo } from 'react';
import styles from './contact-information.module.css';
import { Field } from 'formik';
import InputField from '../input-field';
import SelectField from '../select-field';
import { Option } from '../select-field/select-field.type';

const options: Option[] = [
  {
    value: 6,
    name: '6 month'
  },
  {
    value: 12,
    name: '12 month'
  },
  {
    value: 18,
    name: '18 month'
  },
  {
    value: 24,
    name: '24 month'
  }
];

interface ContactInformationProps {}

const ContactInformation: FC<ContactInformationProps> = () => {
  const date = new Date();
  date.setFullYear(date.getFullYear() - 18);
  const maxDate = date.toLocaleDateString('en-ca');

  return (
    <div className={styles.contactInformation}>
      <h1 className={styles.contact_information_title}>Contact Information</h1>
      <div className={styles.contact_information_block}>
        <InputField
          title="Your last name"
          placeholder="For Example Doe"
          name="lastName"
          validateString={(value) => {
            if (!value?.trim()) {
              return 'Enter your last name';
            }
            return undefined;
          }}
          required
        />
        <InputField
          title="Your first name"
          placeholder="For Example Jhon"
          name="firstName"
          validateString={(value) => {
            if (!value?.trim()) {
              return 'Enter your first name';
            }
            return undefined;
          }}
          required
        />
        <InputField
          title="Your patronymic"
          placeholder="For Example Victorovich"
          name="middleName"
        />
        <SelectField title="Select term" name="term" options={options} type="number" required />
        <InputField
          title="Your email"
          placeholder="test@gmail.com"
          name="email"
          validateString={(value) => {
            if (!value?.trim()) {
              return 'Enter your email';
            }
            if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
              return 'Incorrect email address';
            }
            return undefined;
          }}
          required
        />
        <InputField
          title="Your date of birth"
          placeholder="Select Date and Time"
          name="birthdate"
          max={maxDate}
          validateString={(value) => {
            if (!value?.trim()) {
              return 'Enter your date of birth';
            }
            if (new Date(maxDate).getTime() < new Date(value).getTime()) {
              return 'You are very young';
            }
            return undefined;
          }}
          type="date"
          required
        />
        <InputField
          title="Your passport series"
          placeholder="0000"
          name="passportSeries"
          type="number"
          validateNumber={(value) => {
            if (!value) {
              return 'Enter your passport series';
            }
            if (Number(value) < 10000) {
              return 'The series must be 4 digits';
            }
            return undefined;
          }}
          required
        />
        <InputField
          title="Your passport number"
          placeholder="000000"
          name="passportNumber"
          type="number"
          validateNumber={(value) => {
            if (!value) {
              return 'Enter your passport number';
            }
            if (value.toLocaleString().length != 6) {
              return 'The series must be 6 digits';
            }
            return undefined;
          }}
          required
        />
      </div>
    </div>
  );
};

export default ContactInformation;
