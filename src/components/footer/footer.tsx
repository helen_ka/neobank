import React, { FC } from 'react';
import styles from './index.module.css';
import { LinkFooter } from '../../types';
import FooterNavigation from './footer-navigation';

interface FooterProps {
  linksfooter: LinkFooter[];
}

const Footer: FC<FooterProps> = ({ linksfooter }) => {
  return (
    <footer className={styles.footer}>
      <div className={styles.footer_block}>
        <div className={styles.footer_top}>
          <div className={styles.footer_logo}>
            <img src="/logo.png" />
          </div>
          <div className={styles.footer_contacts}>
            <div className={styles.footer_phone}>+7 (495) 984 25 13</div>
            <div className={styles.footer_neoEmail}>info@neoflex.ru</div>
          </div>
        </div>
        <FooterNavigation linksfooter={linksfooter} />

        <div className={styles.footer_line}></div>
        <div className={styles.footer_endText}>
          We use cookies to personalize our services and improve the user experience of our website.
          Cookies are small files containing information about previous visits to a website. If you
          do not want to use cookies, please change your browser settings
        </div>
      </div>
    </footer>
  );
};

export default Footer;
