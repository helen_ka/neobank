export interface PrescoringType {
  number: number;
  description: string;
}
