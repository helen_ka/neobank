export type { Link } from './Link.type';
export type { RateCond } from './Rate.type';
export type { AboutCard } from './AboutCard.type';
export type { AboutCardBottom } from './AboutCardBottom.type';
export type { CashbackBlocks } from './Cashback.type';
export type { FAQ } from './FAQ.type';
export type { LoanTop } from './LoanTop.type';
export type { PrescoringType } from './Prescoring.type';
export type { LinkFooter } from './LinkFooter.type';
export type { Benefit } from './Benefit.type';
export type { PrescoringForm } from './PrescoringForm.type';
export type { Card } from './Card.type';
