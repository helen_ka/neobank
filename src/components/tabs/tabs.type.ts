export interface Tab {
  key: string;
  name?: string;
  component: JSX.Element;
}
