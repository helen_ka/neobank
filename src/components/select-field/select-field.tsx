import React, { FC, useCallback, useMemo } from 'react';
import { Field, useFormik } from 'formik';
import { ErrorField, ExpandUp, SuccessField } from '../../assets';
import styles from './select-field.module.css';
import { Option } from './select-field.type';

interface SelectFieldProps
  extends Omit<React.SelectHTMLAttributes<HTMLSelectElement>, 'name' | 'title'> {
  name: string;
  title?: string;
  validate?: (value: string) => any;
  options?: Option[];
  type?: 'string' | 'number';
}

const SelectField: FC<SelectFieldProps> = ({
  name,
  title,
  required,
  validate,
  options = [],
  type = 'string',
  ...props
}) => {
  return (
    <Field name={name} validate={validate}>
      {({ field, form: { setFieldValue }, meta: { touched, error } }: any) => (
        <div className={styles.select_field}>
          {title && (
            <label className={styles.select_field__label} htmlFor={name} data-required={required}>
              {title}
            </label>
          )}
          <div className={styles.select_field_block}>
            <select
              {...props}
              {...field}
              onChange={(e) => {
                const value = e.target.value;
                const formattedValue = type === 'number' ? Number(value) : String(value);
                setFieldValue(field.name, formattedValue);
              }}
              required={required}
              style={error && touched ? { borderColor: '#FF5631' } : undefined}
            >
              {options?.map((option) => (
                <option key={option.name} value={option.value}>
                  {option.name}
                </option>
              ))}
            </select>
            <div className={styles.select_field__icon}>
              <ExpandUp />
            </div>
          </div>
        </div>
      )}
    </Field>
  );
};

export default SelectField;
