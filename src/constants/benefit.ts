import { Benefit } from '../types';

export const benefits: Benefit[] = [
  {
    point: 'Powerfull online protection.'
  },
  {
    point: 'Cashback without borders.'
  },
  {
    point: 'Personal design'
  },
  {
    point: 'Work anywhere in the world'
  }
];
