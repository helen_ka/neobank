import { CashbackBlocks } from '../types';

export const cashbacks: CashbackBlocks[] = [
  {
    category: 'For food delivery, cafes and restaurants',
    percent: '5%',
    color: '#EAECEE'
  },
  {
    category: 'In supermarkets with our subscription',
    percent: '5%',
    color: 'rgba(136, 179, 184, 0.6)'
  },
  {
    category: 'In clothing stores and childrens goods',
    percent: '2%',
    color: '#EAECEE'
  },
  {
    category: 'Other purchases and payment of services and\n' + 'fines',
    percent: '1%',
    color: 'rgba(136, 179, 184, 0.6)'
  },
  {
    category: 'Shopping in online stores',
    percent: 'up to 3%',
    color: '#EAECEE'
  },
  {
    category: 'Purchases from our partners',
    percent: '30%',
    color: 'rgba(136, 179, 184, 0.6)'
  }
];
