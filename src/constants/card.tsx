import { Card } from '../types';
import React from 'react';

export const cards: Card[] = [
  {
    img: '/images/CardImage1.png'
  },
  {
    img: '/images/CardImage2.png'
  },
  {
    img: '/images/CardImage3.png'
  },
  {
    img: '/images/CardImage4.png'
  }
];
