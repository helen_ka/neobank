import React, { FC } from 'react';
import { MapImg } from '../../assets';
import styles from './index.module.css';

interface MapProps {}

const Map: FC<MapProps> = () => {
  return (
    <section className={styles.map}>
      <h1 className={styles.map_caption}>You can use our services anywhere in the world</h1>
      <h2 className={styles.map_text}>
        Withdraw and transfer money online through our application
      </h2>
      <div className={styles.map_mapImg}>
        <MapImg />
      </div>
    </section>
  );
};

export default Map;
