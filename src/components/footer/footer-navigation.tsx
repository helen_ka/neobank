import styles from './index.module.css';
import { NavLink } from 'react-router-dom';
import React, { FC } from 'react';
import { LinkFooter } from '../../types';

interface FooterNavigationProps {
  linksfooter: LinkFooter[];
}
const FooterNavigation: FC<FooterNavigationProps> = ({ linksfooter }) => {
  return (
    <nav>
      <ul className={styles.nav}>
        {linksfooter.map((linkfooter) => (
          <li key={linkfooter.url}>
            <NavLink to={linkfooter.url} className={styles.navLink}>
              {linkfooter.name}
            </NavLink>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default FooterNavigation;
