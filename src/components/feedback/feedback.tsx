import React, { FC, useCallback, useState } from 'react';
import './index.module.css';
import { Email, Send } from '../../assets';
import axios from 'axios';
import styles from './index.module.css';
import { Form, Formik } from 'formik';
import InputField from '../input-field';
import Loading from '../loading';

interface FeedbackProps {}

const Feedback: FC<FeedbackProps> = () => {
  const [isSendEmail, setSendEmail] = useState<boolean>(!!localStorage.getItem('sendEmail'));
  const [loading, setLoading] = useState(false);

  const save = useCallback(() => {
    localStorage.setItem('sendEmail', 'true');
    setSendEmail(true);
  }, []);

  const submitHandler = useCallback<React.FormEventHandler<HTMLFormElement>>((data) => {
    setLoading(true);
    axios
      .post('http://localhost:8080/email', data)
      .then(() => save())
      .catch(() => alert('error'))
      .finally(() => setLoading(false));
  }, []);

  return (
    <section className={styles.feedback}>
      <h1 className={styles.feedback_support}>
        <a>Support</a>
      </h1>
      <h1 className={styles.feedback_subscribe}>
        <a>Subscribe Newsletter & get</a>
      </h1>
      <h1 className={styles.feedback_bankNews}>
        <a>Bank News</a>
      </h1>
      {isSendEmail ? (
        <div className={styles.feedback_message}>
          {"You are already subscribed to the bank's newsletter"}
        </div>
      ) : (
        <Formik initialValues={{} as any} onSubmit={submitHandler}>
          {(form) => (
            <div className={styles.feedback_form}>
              <Loading active={loading}>
                <Form className={styles.feedback_blockEmail}>
                  <div className={styles.feedback_blockEmail_block}>
                    <div className={styles.feedback_blockEmail_emailImg}>
                      <Email />
                    </div>
                    <div className={styles.feedback_blockEmail_yourEmail}>
                      <InputField
                        name="email"
                        placeholder="Your Email"
                        validate={(value) => {
                          if (!value) {
                            return 'Enter your email';
                          }
                          if (
                            !/^[A-Z0-9._%+-]+@[A-ZА-Я0-9.-]+\.[A-ZА-Я]{2,4}$/i.test(
                              value.toString()
                            )
                          ) {
                            return 'Incorrect email address';
                          }
                          return undefined;
                        }}
                      />
                    </div>
                    <div className={styles.feedback_blockEmail_subs}>
                      <button
                        disabled={!form.isValid}
                        type="submit"
                        className={styles.feedback_blockEmail_subsButton}
                      >
                        Subscribe
                        <div className={styles.feedback_blockEmail_subsImg}>
                          <Send />
                        </div>
                      </button>
                    </div>
                  </div>
                </Form>
              </Loading>
            </div>
          )}
        </Formik>
      )}
    </section>
  );
};

export default Feedback;
