import React, { FC } from 'react';
import Loantop from '../../components/loantop';
import Tabs from '../../components/tabs';
import Aboutcard from '../../components/aboutcard';
import Rates from '../../components/rates';
import Cashback from '../../components/cashback';
import { rates } from '../../constants/rates';
import { abouts } from '../../constants/aboutcard';
import { cashbacks } from '../../constants/cashback';
import { faqs, faqs2 } from '../../constants/faq';
import { aboutsbottom } from '../../constants/aboutcardbottom';
import { loantops } from '../../constants/loantop';
import Prescoring from '../../components/prescoring';
import { prescorings } from '../../constants/prescoring';
import Questions from '../../components/faq';
import styles from '../home/index.module.css';

interface LoanProps {}

const LoanPage: FC<LoanProps> = () => {
  return (
    <>
      <div className={styles.main_body}>
        <Loantop loantops={loantops} />
        <Tabs
          tabs={[
            {
              key: 'About card',
              name: 'About card',
              component: <Aboutcard abouts={abouts} aboutsbottom={aboutsbottom} />
            },
            {
              key: 'PageWrapper and conditions',
              name: 'Rates and conditions',
              component: <Rates rates={rates} />
            },
            { key: 'Cashback', name: 'Cashback', component: <Cashback cashbacks={cashbacks} /> },
            {
              key: 'FAQ',
              name: 'FAQ',
              component: (
                <>
                  <Questions title="Issuing and receiving a card" questions={faqs} />
                  <Questions title="Using a credit card" questions={faqs2} />
                </>
              )
            }
          ]}
        />
        <Prescoring prescorings={prescorings} />
      </div>
    </>
  );
};

export default LoanPage;
