import React, { FC } from 'react';
import styles from './index.module.css';
import { CashbackBlocks } from '../../types';

interface CashbackProps {
  cashbacks: CashbackBlocks[];
}

const Cashback: FC<CashbackProps> = ({ cashbacks }) => {
  return (
    <section className={styles.cashback}>
      <ul className={styles.cashback_list}>
        {cashbacks.map((cashback) => (
          <li key={cashback.category}>
            <div className={styles.cashback_list_item} style={{ '--color': cashback.color } as any}>
              <div className={styles.cashback_list_category}>{cashback.category}</div>
              <div className={styles.cashback_list_percent}>{cashback.percent}</div>
            </div>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default Cashback;
