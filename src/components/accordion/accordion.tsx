import React, { FC, ReactNode, useCallback, useEffect, useState } from 'react';
import styles from './accordion.module.css';
import { ExpandUp } from '../../assets';

interface AccordionProps {
  title: ReactNode;
  content: ReactNode;
  defaultValue?: boolean;
  onChange?: (value: boolean) => void;
}

const Accordion: FC<AccordionProps> = ({ defaultValue = false, title, content, onChange }) => {
  const [visible, setVisible] = useState<boolean | undefined>(defaultValue);

  const change = useCallback(() => {
    setVisible((currentValue) => {
      onChange?.(!currentValue);
      return !currentValue;
    });
  }, [onChange]);

  return (
    <div className={`${styles.accordion} ${visible ? 'active' : ''}`}>
      <div className={styles.accordion__title} onClick={change}>
        <div className={styles.accordion__title__text}>{title}</div>
        <div className={styles.accordion__title__marker}>
          <ExpandUp />
        </div>
      </div>
      <div className={styles.accordion__content}>{content}</div>
    </div>
  );
};

export default Accordion;
