import React, { FC, PropsWithChildren } from 'react';
import styles from './tooltip.module.css';

interface TooltipProps {
  message: string;
}

const Tooltip: FC<PropsWithChildren<TooltipProps>> = ({ children, message }) => {
  return (
    <div className={styles.tooltip}>
      <div className={styles.tooltip__message}>
        <div className={styles.tooltip__message__text}>{message}</div>
      </div>
      {children}
    </div>
  );
};

export default Tooltip;
