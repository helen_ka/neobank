import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import HomePage from './modules/home';
import '@fontsource/ubuntu';
import '@fontsource/rubik';
import LoanPage from './modules/loan';
import PageWrapper from './components/page-wrapper';
import { links } from './constants/links';
import { linksfooter } from './constants/linksfooter';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <PageWrapper links={links} linksfooter={linksfooter}>
        <Routes>
          <Route path="home" element={<HomePage />} />
          <Route path="loan" element={<LoanPage />} />
          <Route path="*" element={<Navigate to="/home" />} />
        </Routes>
      </PageWrapper>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
