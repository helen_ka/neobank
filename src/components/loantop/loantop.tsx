import React, { FC } from 'react';
import styles from './index.module.css';
import { LoanTop } from '../../types';
import Tooltip from '../tooltip';

interface LoanProps {
  loantops: LoanTop[];
}

const Loantop: FC<LoanProps> = ({ loantops }) => {
  return (
    <section className={styles.loantop}>
      <div className={styles.loantop_textBlock}>
        <h1 className={styles.loantop_caption}>Platinum digital credit card</h1>
        <h2 className={styles.loantop_text}>
          Our best credit card. Suitable for everyday spending and shopping. Cash withdrawals and
          transfers without commission and interest.
        </h2>
        <ul className={styles.loantop_points}>
          {loantops.map((loantop) => (
            <Tooltip message={loantop.message} key={loantop.message}>
              <li key={loantop.condition}>
                <div className={styles.points_item}>
                  <h1 className={styles.loantop_points_condition}>{loantop.condition}</h1>
                  <h2 className={styles.loantop_points_text}>{loantop.description}</h2>
                </div>
              </li>
            </Tooltip>
          ))}
        </ul>
        <a href="#prescoring" className={styles.loantop_button_apply_to}>
          <button className={styles.loantop_button_apply}>Apply for card</button>
        </a>
      </div>
      <div className={styles.loantop_cardBlock}>
        <div className={styles.loantop_imgCard}>
          <img src="images/CardImage1.png" />
        </div>
      </div>
    </section>
  );
};

export default Loantop;
