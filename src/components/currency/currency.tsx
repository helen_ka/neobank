import React, { FC, useCallback, useEffect, useMemo, useState, useRef } from 'react';
import axios from 'axios';
import styles from './index.module.css';

import { HOST, INTERVAL, KEY, CURRENT_CURRENCIES, MAIN_CURRENCIES } from '../../constants/constant';

const loadCurrency = async ({ from, to }: Currencies) => {
  return axios
    .get(`https://${HOST}/exchange`, {
      params: { from, to, q: '1.0' },
      headers: {
        'X-RapidAPI-Key': KEY,
        'X-RapidAPI-Host': HOST
      }
    })
    .then(({ data, headers: { date } }) => ({ from, to, value: data, date } as CurrencyData));
};

const loadCurrencies = async () => {
  const formattedListQuotes = [...MAIN_CURRENCIES].map((value) => ({
    from: value,
    to: CURRENT_CURRENCIES
  }));

  return await Promise.all(formattedListQuotes.map(loadCurrency));
};

interface Currencies {
  from: string;
  to: string;
}

interface CurrencyData {
  from: string;
  to: string;
  value: number;
  date: string;
}

interface CurrencyProps {}

const Currency: FC<CurrencyProps> = () => {
  const [data, setData] = useState<CurrencyData[]>([]);

  const lastDate = useMemo(() => {
    const currentDate = new Date(data[data.length - 1]?.date);
    return 'MSC ' + currentDate.toLocaleDateString('ru-RU');
  }, [data]);

  const timeIntervalCode = useRef<NodeJS.Timer>();

  useEffect(() => {
    loadCurrencies().then((newData) => setData(newData));

    timeIntervalCode.current = setInterval(() => {
      loadCurrencies().then((newData) => setData(newData));
    }, INTERVAL);
    return () => timeIntervalCode.current && clearInterval(timeIntervalCode.current);
  }, []);

  return (
    <section className={styles.currency}>
      <div className={styles.currency_textBlock}>
        <h1 className={styles.currency_caption}>Exchange rate in internet bank</h1>
        <h2 className={styles.currency_textCurrency}>Currency</h2>
        <div className={styles.currency_list} id="cur_list">
          <>
            {data.map(({ from, to, value }) => (
              <div
                className={styles.currency_list_item}
                id={`currency_${from}_${to}`}
                key={`currency_${from}_${to}`}>
                <div className={styles.currency_list_name}>{`${from}:`}</div>
                <div className={styles.currency_list_price}>{`${value.toFixed(2)}₽`}</div>
              </div>
            ))}
          </>
        </div>
        <div className={styles.currency_textCourses}>
          <h3>
            <a>All courses</a>
          </h3>
        </div>
      </div>
      <div className={styles.currency_bankText}>
        <div className={styles.currency_textInfo}>
          {'Update every 15 minutes,  '}
          {lastDate}
        </div>
        <div className={styles.currency_bank}>
          <img src="images/bank.png" />
        </div>
      </div>
    </section>
  );
};

export default Currency;
