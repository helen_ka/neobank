import { ReactNode } from 'react';

export interface AboutCardBottom {
  color: string;
  icon: ReactNode;
  caption: string;
  description: string;
}
