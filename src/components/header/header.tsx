import React, { FC } from 'react';
import styles from './index.module.css';
import { NavLink } from 'react-router-dom';
import { Link } from '../../types';

interface HeaderProps {
  links: Link[];
}

const Header: FC<HeaderProps> = ({ links }) => {
  return (
    <header className={styles.header}>
      <div className={styles.NeoBank}>
        <a href={'/home'}> NeoBank </a>
      </div>
      <nav>
        <ul className={styles.nav}>
          {links.map((link) => (
            <li key={link.url}>
              <NavLink
                to={link.url}
                className={styles.navLink}
                style={(isActive) => ({
                  color: isActive ? '#1d1929' : '#B2A35F'
                })}>
                {link.name}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>
      <button className={styles.OnlineBank}>Online Bank</button>
    </header>
  );
};

export default Header;
