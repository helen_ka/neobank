import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';
import styles from './tabs.module.css';
import { Tab } from './tabs.type';
import Divider from '../divider';

interface TabsProps {
  tabs: Tab[];
  defaultTabKey?: string;
  value?: string;
  onChange?: (key: string) => void;
}

const Tabs: FC<TabsProps> = ({ tabs, defaultTabKey, value, onChange }) => {
  const [currentTabKey, setCurrentTabKey] = useState<string | undefined>(
    defaultTabKey ?? tabs[0]?.key
  );

  const currentTab = useMemo(
    () => tabs.find(({ key }) => key === currentTabKey)?.component,
    [currentTabKey, tabs]
  );

  const openTab = useCallback(
    (key: string) => {
      onChange?.(key);
      setCurrentTabKey(key);
    },
    [onChange]
  );

  useEffect(() => {
    if (value) {
      setCurrentTabKey(value);
    }
  }, [value]);

  return (
    <div className={styles.tabs}>
      <div className={styles.tab_menu}>
        {tabs?.map((item) => (
          <div
            key={item.key}
            className={`${styles.tab_menu__item} ${item.key === currentTabKey && 'active'}`}
            onClick={() => openTab(item.key)}>
            {item.name ?? item.key}
          </div>
        ))}
      </div>
      <Divider />
      <div className={styles.content}>{currentTab}</div>
    </div>
  );
};

export default Tabs;
