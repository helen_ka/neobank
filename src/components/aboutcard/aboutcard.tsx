import React, { FC } from 'react';
import styles from './index.module.css';
import { AboutCard } from '../../types';
import { AboutCardBottom } from '../../types/AboutCardBottom.type';
import { aboutsbottom } from '../../constants/aboutcardbottom';

interface AboutcardProps {
  abouts: AboutCard[];
  aboutsbottom: AboutCardBottom[];
}

const Aboutcard: FC<AboutcardProps> = ({ abouts }) => {
  return (
    <section className={styles.aboutcard}>
      <ul className={styles.aboutcard_smallBlocks}>
        {abouts.map((about) => (
          <li key={about.caption}>
            <div className={styles.aboutcard_smallBlocks_item} style={{ background: about.color }}>
              <div className={styles.aboutcard_img}>{about.icon}</div>
              <div className={styles.aboutcard_caption}>{about.caption}</div>
              <div className={styles.aboutcard_description}>{about.description}</div>
            </div>
          </li>
        ))}
      </ul>
      <ul className={styles.aboutcard_bigBlocks}>
        {aboutsbottom.map((about) => (
          <li key={about.caption}>
            <div className={styles.aboutcard_bigBlocks_item} style={{ background: about.color }}>
              <div className={styles.aboutcard_img}>{about.icon}</div>
              <div className={styles.aboutcard_caption}>{about.caption}</div>
              <div className={styles.aboutcard_description}>{about.description}</div>
            </div>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default Aboutcard;
