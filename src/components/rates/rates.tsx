import React, { FC } from 'react';
import styles from './index.module.css';
import { RateCond } from '../../types';

interface RatesProps {
  rates: RateCond[];
}

const Rates: FC<RatesProps> = ({ rates }) => {
  return (
    <section className={styles.rates}>
      <ul className={styles.rates_list}>
        {rates.map((rate) => (
          <li key={rate.title}>
            <div className={styles.rates_list_item}>
              <div className={styles.rates_list_title}>{rate.title}</div>
              <div className={styles.rates_list_text}>{rate.text}</div>
            </div>
            <div className={styles.rates_line}></div>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default Rates;
