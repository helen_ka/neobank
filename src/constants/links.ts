import { Link } from '../types';

export const links: Link[] = [
  {
    url: '/loan',
    name: 'Credit card'
  },
  {
    url: '/product',
    name: 'Product'
  },
  {
    url: '/account',
    name: 'Account'
  },
  {
    url: '/resources',
    name: 'Resources'
  }
];
