import { LinkFooter } from '../types';

export const linksfooter: LinkFooter[] = [
  {
    url: '/About bank',
    name: 'About bank'
  },
  {
    url: '/Ask a Question',
    name: 'Ask a Question'
  },
  {
    url: '/Quality of service',
    name: 'Quality of service'
  },
  {
    url: '/Requisites',
    name: 'Requisites'
  },
  {
    url: '/Press center',
    name: 'Press center'
  },
  {
    url: '/Bank career',
    name: 'Bank career'
  },
  {
    url: '/Investors',
    name: 'Investors'
  },
  {
    url: '/Analytics',
    name: 'Analytics'
  },
  {
    url: '/Business and processes',
    name: 'Business and processes'
  },
  {
    url: '/Compliance and business ethics',
    name: 'Compliance and business ethics'
  }
];
