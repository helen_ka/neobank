export interface CashbackBlocks {
  category: string;
  percent: string;
  color: string;
}
