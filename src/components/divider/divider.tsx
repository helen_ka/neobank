import React, { FC } from 'react';
import styles from './divider.module.css';

const Divider: FC<React.HTMLAttributes<HTMLDivElement>> = ({ className, ...props }) => (
  <div className={`${className} ${styles.divider}`} {...props} />
);

export default Divider;
