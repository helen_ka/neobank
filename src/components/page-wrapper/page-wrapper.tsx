import React, { FC, PropsWithChildren } from 'react';
import { Link, LinkFooter } from '../../types';
import Header from '../header';
import Footer from '../footer';

interface PageWrapperProps {
  links: Link[];
  linksfooter: LinkFooter[];
}

const PageWrapper: FC<PropsWithChildren<PageWrapperProps>> = ({ links, linksfooter, children }) => {
  return (
    <>
      <Header links={links} />
      {children}
      <Footer linksfooter={linksfooter} />
    </>
  );
};

export default PageWrapper;
