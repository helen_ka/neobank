import React, { FC } from 'react';
import styles from './index.module.css';

interface NewsBlockProps {
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  onError: (e: any) => void;
}

const NewsBlock: FC<NewsBlockProps> = ({ title, description, url, urlToImage, onError }) => {
  return (
    <a className={styles.news_list_item} href={url} key={url} target="_blank" rel="noreferrer">
      <div className={styles.news_list_item_img}>
        <img src={urlToImage} onError={onError} />
      </div>
      <div className={styles.news_list_item_title}>{title}</div>
      <div className={styles.news_list_item_text}>{description}</div>
    </a>
  );
};

export default NewsBlock;
