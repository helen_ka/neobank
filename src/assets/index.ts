import { ReactComponent as LogoReact } from './logoreact.svg';
import { ReactComponent as Illustration } from './illustration.svg';
import { ReactComponent as MapImg } from './map.svg';
import { ReactComponent as Email } from './email.svg';
import { ReactComponent as Send } from './send.svg';
import { ReactComponent as LeftButton } from './left-button.svg';
import { ReactComponent as Money } from './money.svg';
import { ReactComponent as Calendar } from './calendar.svg';
import { ReactComponent as Clock } from './clock.svg';
import { ReactComponent as Bag } from './bag.svg';
import { ReactComponent as CreditCard } from './creditCard.svg';
import { ReactComponent as ExpandUp } from './expand_up.svg';
import { ReactComponent as Spinner } from './spinner.svg';
import { ReactComponent as Tick } from './tick.svg';
import { ReactComponent as ErrorField } from './error-field.svg';
import { ReactComponent as SuccessField } from './success-field.svg';

export {
  LogoReact,
  Illustration,
  MapImg,
  Email,
  Send,
  LeftButton,
  Money,
  Clock,
  Calendar,
  Bag,
  CreditCard,
  ExpandUp,
  Spinner,
  Tick,
  ErrorField,
  SuccessField
};
