import React, { FC } from 'react';
import { FAQ } from '../../types';
import styles from './questions.module.css';
import Accordion from '../accordion';

interface QuestionsProps {
  title: string;
  questions: FAQ[];
}

const Questions: FC<QuestionsProps> = ({ title, questions }) => {
  return (
    <div className={styles.questions}>
      <h1 className={styles.questions__title}>{title}</h1>
      {questions?.map((question) => (
        <Accordion key={question.title} {...question} />
      ))}
    </div>
  );
};

export default Questions;
