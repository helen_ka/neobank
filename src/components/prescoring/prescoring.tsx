import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';
import styles from './index.module.css';
import { PrescoringForm, PrescoringType } from '../../types';
import { Formik, Form, Field } from 'formik';
import ContactInformation from '../contact-information';
import axios from 'axios';
import Loading from '../loading';
import InputField from '../input-field';

interface PrescoringProps {
  prescorings: PrescoringType[];
}

const Prescoring: FC<PrescoringProps> = ({ prescorings }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [amount, setAmount] = useState<number>(150000);

  const sendForm = useCallback(
    (data: PrescoringForm) => {
      setLoading(true);
      axios
        .post('http://localhost:8080/application', data)
        .then(() => setLoading(false))
        .catch(() => alert('error'))
        .finally(() => setLoading(false));
    },
    [setLoading]
  );

  const submitHandler = useCallback(
    (event: PrescoringForm) => {
      console.log(event);
      sendForm(event);
    },
    [sendForm]
  );

  function changeAmount(event: any) {
    setAmount(event.target.value);
  }

  return (
    <Loading active={loading}>
      <section className={styles.prescoring} id="prescoring">
        <h1 className={styles.prescoring_head_caption}>How to get a card</h1>
        <ul className={styles.prescoring_list}>
          {prescorings.map((prescoring) => (
            <li key={prescoring.number}>
              <div className={styles.prescoring_list_item}>
                <div className={styles.prescoring_list_item_top}>
                  <div className={styles.prescoring_circle}></div>
                  <div className={styles.prescoring_line}></div>
                </div>
                <div className={styles.prescoring_list_item_bottom}>
                  <div className={styles.prescoring_list_number}>{prescoring.number}</div>
                  <div className={styles.prescoring_list_description}>{prescoring.description}</div>
                </div>
              </div>
            </li>
          ))}
        </ul>
        <Formik initialValues={{ term: 6, amount } as PrescoringForm} onSubmit={submitHandler}>
          {() => (
            <Form>
              <div className={styles.prescoring_block}>
                <div className={styles.prescoring_block_top}>
                  <div className={styles.prescoring_steps}>
                    <div className={styles.prescoring_steps_top}>
                      <h1 className={styles.prescoring_caption}>Customize your card</h1>
                      <h2 className={styles.prescoring_text}>Step 1 of 5</h2>
                    </div>
                    <div className={styles.prescoring_steps_bottom}>
                      <div className={styles.prescoring_steps_bottom_select}>
                        <InputField
                          id="amount_value"
                          className={styles.prescoring_steps_bottom_amount}
                          title="Select amount"
                          placeholder="For Example 150000"
                          name="amount"
                          value={amount}
                          min="15000"
                          max="600000"
                          type="number"
                          onChange={changeAmount}
                          validateNumber={(value) => {
                            if (!value) {
                              return 'Enter selected amount';
                            }
                            if (+value < 15000|| +value > 600000) {
                              return 'Enter the correct amount';
                            }
                            return undefined;
                          }}
                          required
                        />
                      </div>
                      <div className={styles.prescoring_steps_bottom_line}></div>
                      <div className={styles.prescoring_steps_bottom_circle}></div>
                      <div className={styles.prescoring_steps_bottom_sum}>
                        <h2 className={styles.prescoring_steps_bottom_sum_minmax}>15 000</h2>
                        <h2 className={styles.prescoring_steps_bottom_sum_minmax}>600 000</h2>
                      </div>
                    </div>
                  </div>
                  <div className={styles.prescoring_dividing_line}></div>
                  <div className={styles.prescoring_result}>
                    <h1 className={styles.prescoring_result_caption}>You have chosen the amount</h1>
                    <h2 className={styles.prescoring_result_amount}>{amount} P</h2>
                    <div className={styles.prescoring_result_line}></div>
                  </div>
                </div>
                <ContactInformation />
                <div className={styles.prescoring_button}>
                  <button type="submit" className={styles.prescoring_button_continue}>
                    Continue
                  </button>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </section>
    </Loading>
  );
};

export default Prescoring;
