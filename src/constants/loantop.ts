import { LoanTop } from '../types';

export const loantops: LoanTop[] = [
  {
    message: 'When repaying the full\n' + 'debt up to 160 days.',
    condition: 'Up to 160 days',
    description: 'No percent'
  },
  {
    message: 'Over the limit willaccrue percent',
    condition: 'Up to 600 000 ₽',
    description: 'Credit limit'
  },
  {
    message: 'Promotion valid until December 31, 2022.',
    condition: '0 ₽',
    description: 'Card service is free'
  }
];
