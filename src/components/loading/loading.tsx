import React, { FC, PropsWithChildren } from 'react';
import styles from './loading.module.css';
import { Spinner } from '../../assets';

interface LoadingProps {
  active: boolean;
}

const Loading: FC<PropsWithChildren<LoadingProps>> = ({ children, active = false }) => {
  return (
    <div className={styles.loading}>
      {active && (
        <div className={styles.loading__mask}>
          <div className={styles.loading__mask__icon}>
            <Spinner />
          </div>
        </div>
      )}
      {children}
    </div>
  );
};

export default Loading;
