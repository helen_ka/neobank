import React, {
  FC,
  useCallback,
  useEffect,
  useState,
  useRef,
  ReactEventHandler,
  useMemo
} from 'react';
import axios from 'axios';
import { LeftButton } from '../../assets';
import styles from './index.module.css';
import NewsBlock from './news-block';
import { NEWS_HOST, API_KEY, NEWS_INTERVAL } from '../../constants/constant';

const loadNews = async () => {
  return axios
    .get(`${NEWS_HOST}/v2/top-headlines`, {
      params: queryParams
    })
    .then<NewsData[]>(({ data: { articles } }) =>
      articles
        .map(({ title, description, url, urlToImage }: NewsData) => ({
          title,
          description,
          url,
          urlToImage
        }))
        .filter(
          ({ description }: any) => description != null && !/<\/?[a-z][\s\S]*>/gm.test(description)
        )
    );
};

const queryParams = {
  country: 'us',
  category: 'business',
  apiKey: API_KEY,
  pageSize: 25
};

interface NewsData {
  title: string;
  description: string;
  url: string;
  urlToImage: string;
}

interface NewsProps {}

const News: FC<NewsProps> = () => {
  const [data, setData] = useState<NewsData[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [maxPages, setMaxPages] = useState<number>(0);

  const content = useRef<HTMLDivElement>(null);

  const prevHandler = useCallback(() => {
    setCurrentPage((prevValue) => prevValue - 1);
  }, []);

  const nextHandler = useCallback(() => {
    setCurrentPage((prevValue) => prevValue + 1);
  }, []);

  const onErrorHandler = useCallback<ReactEventHandler<HTMLImageElement>>((event) => {
    const parent = event.currentTarget.closest('a');
    if (parent) {
      parent.remove();

      setMaxPages((prevValue) => prevValue - 1);
    }
  }, []);

  useEffect(() => {
    loadNews().then((news) => setData(news.filter(({ urlToImage }) => !!urlToImage)));
  }, []);

  useEffect(() => {
    if (data) {
      setMaxPages(data.length);
      setCurrentPage(0);
    }
  }, [data]);

  const items = useMemo(() => {
    return data.map(({ title, description, url, urlToImage }) => (
      <NewsBlock
        title={title}
        description={description}
        url={url}
        urlToImage={urlToImage}
        key={title}
        onError={onErrorHandler}
      />
    ));
  }, [data]);

  return (
    <section className={styles.news}>
      <h1 className={styles.news_caption}>Current news from the world of finance</h1>
      <h2 className={styles.news_text}>
        We update the news feed every 15 minutes. You can learn more by clicking on the news you are
        interested in.
      </h2>
      <div className={styles.news_list}>
        <div className={styles.news_content} style={{ '--number': currentPage } as any}>
          {items}
        </div>
      </div>
      <div className={styles.news_buttons}>
        <button
          className={styles.news_buttons_item}
          onClick={prevHandler}
          disabled={currentPage === 0}>
          <LeftButton />
        </button>
        <button
          className={styles.news_buttons_item}
          onClick={nextHandler}
          disabled={currentPage === maxPages - 1}>
          <LeftButton />
        </button>
      </div>
    </section>
  );
};
export default News;
