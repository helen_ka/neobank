import React, { FC, useCallback } from 'react';
import { Field } from 'formik';
import { ErrorField, SuccessField } from '../../assets';
import styles from './input-field.module.css';

interface InputFieldProps
  extends Omit<React.InputHTMLAttributes<HTMLInputElement>, 'name' | 'title'> {
  name: string;
  title?: string;
  validateString?: (value: string) => any;
  validateNumber?: (value: number) => any;
}

const InputField: FC<InputFieldProps> = ({
  name,
  title,
  required,
  validateString,
  validateNumber,
  type,
  ...props
}) => {
  const additionalProps = useCallback(
    ({
      onFocus,
      onBlur
    }: Partial<React.InputHTMLAttributes<HTMLInputElement>>): Partial<
      React.InputHTMLAttributes<HTMLInputElement>
    > => {
      if (type === 'date') {
        return {
          onFocus: (event) => {
            const { target } = event;
            target.type = 'date';
            onFocus?.(event);
          },
          onBlur: (event) => {
            const { target } = event;
            if (target.type === 'date' && (!target.value || target.value.trim() === '')) {
              target.type = 'text';
            }
            onBlur?.(event);
          }
        };
      }
      return { type };
    },
    [type]
  );

  return (
    <Field name={name} validateString={validateString} validateNumber={validateNumber}>
      {({ field: { value = '', ...fieldProps }, meta: { touched, error } }: any) => (
        <div className={styles.input_field}>
          {title && (
            <label className={styles.input_field__label} htmlFor={name} data-required={required}>
              {title}
            </label>
          )}
          <div className={styles.input_field_block}>
            <input
              {...props}
              {...fieldProps}
              {...additionalProps(fieldProps)}
              value={value}
              required={required}
              style={error && touched ? { borderColor: '#FF5631' } : undefined}
            />
            {touched && (
              <div className={styles.input_field__icon}>
                {error ? <ErrorField /> : <SuccessField />}
              </div>
            )}
          </div>
          {error && touched && <div className={styles.input_field__error}>{error}</div>}
        </div>
      )}
    </Field>
  );
};

export default InputField;
